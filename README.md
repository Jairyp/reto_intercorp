# Reto Intercorp

## Descripción
Crear un microservicio el cual gestione la creacion  del cliente,consultas e indicadores

## Objetivo
* Crear proyectos usando: 
    * https://start.spring.io/
    * maven
      ```
      mvn archetype:generate -DgroupId=com.intercorp -DartifactId=intercorp -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
      ``` 

* Como usarlo de Manera Local     
  * ubicarse en la ruta base del proyecto donde se encuentra el archivo \intercorp_reto\docker-compose.yml
  * Luego ejecutar el siguiente comando esto levantara el microservicio con la base de datos
    ```
      docker-compose up
      ``` 
  * validar que los contenedores esten corriendo con el siguiente comando, posterior validacion se puede consumir el microservicio en localhost:8085     

    ```
      docker ps -a
      ``` 
  
![Screenshot](containers.png)

* Spring boot:
    * Arquitectura (Controller, Service, Repository, Entity y Dto)


* Pruebas unitarias:
    * Crear Clientes

    
## Reto 
* crear 3 endpoints para guardar,consultar indicadores y consultar customers:
    * POST /v1/customers
    * GET /v1/customers/consulta?{dni}&{email}
    * GET /v1/customers/indicadores/{indicador}


## Prerequisites
**Software**
* Java SDK 8.x or higher
* Maven 3.x or higher
* Docker 20.10.x or higher
* Docker Compose: 2.2.3 or higher
* IntelliJ IDEA
