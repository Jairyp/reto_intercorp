package com.example.lab01.controller.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intercorp.retail.controller.web.CustomerController;
import com.intercorp.retail.controller.web.dto.CustomerWebDto;
import com.intercorp.retail.core.domain.Customer;
import com.intercorp.retail.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CustomerController.class)
@ContextConfiguration(classes = {CustomerControllerTest.Configuration.class})
public class CustomerControllerTest {

    public static class Configuration {

        @Bean
        public CustomerController customerController(CustomerService customerService){
            return new CustomerController(customerService);
        }

    }

    @MockBean
    private CustomerService customerService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void cleanCachesAndMocksAndStubs() {
        reset(customerService);
    }

    @Test
    void doPostSync_ok() throws Exception {
        // Preparing data
        Customer customer = new Customer();
        customer.setId(1);
        customer.setNombre("nombre1");
        customer.setApellido("paterno1");
        customer.setDni("46435580");
        customer.setEmail("jairyp@gmail.com");
        customer.setFecha_nacimiento("1990-06-09");
        CustomerWebDto customerWebDto = new CustomerWebDto();
        customerWebDto.setCustomer(customer);

        String json = new ObjectMapper().writeValueAsString(customerWebDto);

        // Mocks & Stubs configuration
        doNothing().when(customerService).save(customer);


        // Business logic execution
        mockMvc.perform(post("/v1/customers")
                        .accept(MediaType.APPLICATION_JSON)
                        .header(CustomerController.X_API_FORCE_SYC_HEADER, "true")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());

        // Validating mocks behaviour
        verify(customerService).save(customer);
        verifyNoMoreInteractions(customerService);

        // Validating results
    }

}
