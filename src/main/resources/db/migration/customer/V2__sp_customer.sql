DELIMITER //

CREATE PROCEDURE GetAllCustomers()
BEGIN
SELECT *  FROM customer;
END //

CREATE PROCEDURE GetAllCustomersByEmailOrDni (IN p_dni varchar(30),
                                            IN p_email varchar(30))
BEGIN
SELECT * FROM customer WHERE email = p_email or dni = p_dni;
END //

CREATE PROCEDURE GetCountCustomersForYearAndMonth ()
BEGIN
SELECT MONTHNAME(fecha_nacimiento) AS mes, YEAR(fecha_nacimiento) AS anio, COUNT(*) AS cantidad FROM customer GROUP BY mes,anio;
END //

CREATE PROCEDURE GetMaxCustomerForYearOrMonth()
BEGIN
SELECT COUNT(*) AS cantidad,MONTHNAME(fecha_nacimiento) AS mes,YEAR(fecha_nacimiento) AS anio FROM customer GROUP BY mes , anio ORDER BY cantidad DESC LIMIT 1;
END //

CREATE PROCEDURE GetMinCustomerForYearOrMonth()
BEGIN
SELECT COUNT(*) AS cantidad,MONTHNAME(fecha_nacimiento) AS mes,YEAR(fecha_nacimiento) AS anio FROM customer GROUP BY mes , anio ORDER BY cantidad ASC LIMIT 1;
END //

CREATE PROCEDURE CustomersInsert (IN p_name varchar(30),
                                  IN p_apellido varchar(30),
                                  IN p_mail varchar(30),
                                  IN p_dni varchar(30),
                                  IN p_fecha_crea DATE,
                                  IN p_fecha_nacimiento DATE)
BEGIN
insert into customer (nombre,apellido,email,dni,fecha_crea,fecha_nacimiento)
values(p_name,p_apellido,p_mail,p_dni,p_fecha_crea,p_fecha_nacimiento);
END //


DELIMITER ;