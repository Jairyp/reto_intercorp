

CREATE TABLE IF NOT EXISTS customer (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(30) NOT NULL,
    `apellido` VARCHAR(30) NOT NULL,
    `email` VARCHAR(30) NOT NULL,
    `dni` VARCHAR (8)  NOT NULL,
    `fecha_crea` DATE NOT NULL,
    `fecha_nacimiento` DATE NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY  (`id`),
    INDEX `customer_idx` (`id` ASC)
    );

insert into customer (nombre,apellido,email,dni,fecha_crea,fecha_nacimiento)
values('ROBERTO JAIR','YACTAYO PAUCAR','jairyp90@gmail.com','46435580','1990-06-09','1990-06-09');
