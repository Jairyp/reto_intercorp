package com.intercorp.retail.controller.web.dto;

import com.intercorp.retail.core.domain.Customer;

import java.util.List;

public class CustomersWebDto {

    private List<Customer> customers;

    public CustomersWebDto(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
