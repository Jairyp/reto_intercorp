package com.intercorp.retail.controller.web;

import com.intercorp.retail.controller.web.dto.CustomersWebDto;
import com.intercorp.retail.controller.web.dto.CustomerWebDto;
import com.intercorp.retail.controller.web.dto.IndicatorWebDto;
import com.intercorp.retail.controller.web.dto.IndicatorsWebDto;
import com.intercorp.retail.core.domain.Customer;
import com.intercorp.retail.core.domain.Indicators;
import com.intercorp.retail.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@ApiOperation(value = "/v1/customers", tags = "customers")
@RestController
@RequestMapping("/v1/customers")
public class CustomerController {

    public static final String X_API_FORCE_SYC_HEADER = "X-Api-Force-Sync";

    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ApiOperation(value = "crear customers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS",response = CustomerWebDto.class)
    })
    @PostMapping
    public HttpEntity<CustomerWebDto> create(@RequestBody CustomerWebDto customerWebDto) {
            customerService.save(customerWebDto.getCustomer());
            return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "consulta customers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS",response = CustomersWebDto.class),
            @ApiResponse(code = 404, message = "NOT FOUND")
    })
    @GetMapping("/consulta")
    public HttpEntity<CustomersWebDto> getByParam(@RequestParam(required = false,name = "dni") String dni,
                                                  @RequestParam(required=false, name = "email") String email) {
        List<Customer> customers;
        if(dni!=null || email!=null) {
            customers = customerService.getByDniOrEmail(dni, email);
        } else {
            customers = customerService.getList();
        }

        if(!customers.isEmpty()){
            CustomersWebDto customersWebDto = new CustomersWebDto(customers);
            return new ResponseEntity<>(customersWebDto, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "consulta indicadores customers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS"),
            @ApiResponse(code = 404, message = "NOT FOUND")
    })
    @GetMapping("/indicadores/{indicador}")
    public HttpEntity<?> getIndicadores(@PathVariable String indicador) {
        List<Indicators> indicators;
        Indicators indicator;

        switch (indicador) {
            case "1":
                indicators = customerService.getCountForYearAndMonth();
                IndicatorsWebDto indicatorsWebDto = new IndicatorsWebDto(indicators);
                return new ResponseEntity<>(indicatorsWebDto, HttpStatus.OK);
            case "2": {
                indicator = customerService.getMaxCustomerForYearOrMonth();
                IndicatorWebDto indicatorWebDto = new IndicatorWebDto(indicator);
                return new ResponseEntity<>(indicatorWebDto, HttpStatus.OK);
            }
            case "3": {
                indicator = customerService.getMinCustomerForYearOrMonth();
                IndicatorWebDto indicatorWebDto = new IndicatorWebDto(indicator);
                return new ResponseEntity<>(indicatorWebDto, HttpStatus.OK);
            }
            default:
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }



}
