package com.intercorp.retail.controller.web.dto;

import com.intercorp.retail.core.domain.Indicators;

import java.util.List;

public class IndicatorsWebDto {

    private List<Indicators> indicators;

    public IndicatorsWebDto(List<Indicators> indicators) {
        this.indicators = indicators;
    }

    public List<Indicators> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Indicators> indicators) {
        this.indicators = indicators;
    }
}
