package com.intercorp.retail.controller.web.dto;

import com.intercorp.retail.core.domain.Customer;


public class CustomerWebDto {

    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
