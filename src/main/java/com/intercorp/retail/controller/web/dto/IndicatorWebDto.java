package com.intercorp.retail.controller.web.dto;

import com.intercorp.retail.core.domain.Indicators;

public class IndicatorWebDto {

    private Indicators indicators;

    public IndicatorWebDto(Indicators indicators) {
        this.indicators = indicators;
    }

    public Indicators getIndicators() {
        return indicators;
    }

    public void setIndicators(Indicators indicators) {
        this.indicators = indicators;
    }
}
