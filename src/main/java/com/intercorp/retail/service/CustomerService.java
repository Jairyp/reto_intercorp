package com.intercorp.retail.service;

import com.intercorp.retail.core.domain.Customer;
import com.intercorp.retail.core.domain.Indicators;

import java.util.List;

public interface CustomerService {

    List<Customer> getList();
    List<Customer> getByDniOrEmail(String dni,String email);
    void save(Customer customer);
    List<Indicators> getCountForYearAndMonth();
    Indicators getMaxCustomerForYearOrMonth();
    Indicators getMinCustomerForYearOrMonth();

}
