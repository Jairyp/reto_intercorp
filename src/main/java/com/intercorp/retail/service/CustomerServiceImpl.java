package com.intercorp.retail.service;

import com.intercorp.retail.core.domain.Indicators;
import com.intercorp.retail.repository.CustomerRepository;
import com.intercorp.retail.core.domain.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getList() {
        return customerRepository.getList();
    }

    @Override
    public List<Customer> getByDniOrEmail(String dni, String email) {
        return customerRepository.getByDniOrEmail(dni,email);
    }

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public List<Indicators> getCountForYearAndMonth() {
        return customerRepository.getCountForYearAndMonth();
    }

    @Override
    public Indicators getMaxCustomerForYearOrMonth() {
        return customerRepository.getMaxCustomerForYearOrMonth();
    }

    @Override
    public Indicators getMinCustomerForYearOrMonth() {
        return customerRepository.getMinCustomerForYearOrMonth();
    }
}
