package com.intercorp.retail.core.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Indicators implements Serializable {

    @ApiModelProperty(notes = "mes de nacimiento del cliente", name = "mes",value = "Julio")
    private String mes;

    @ApiModelProperty(notes = "año de nacimiento del cliente", name = "anio",value = "1990")
    private String anio;

    @ApiModelProperty(notes = "cantidad", name = "cantidad",value = "10")
    private String cantidad;

    @ApiModelProperty(notes = "natalidad", name = "natalidad",value = "10")
    private String natalidad;

    public Indicators() {
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getNatalidad() {
        return natalidad;
    }

    public void setNatalidad(String natalidad) {
        this.natalidad = natalidad;
    }
}
