package com.intercorp.retail.core.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.sql.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Customer implements Serializable {

    private int id;

    @ApiModelProperty(notes = "nombre del cliente", name = "nombre",value = "Roberto")
    private String nombre;

    @ApiModelProperty(notes = "apellido del cliente", name = "apellido",value = "Roberto")
    private String apellido;

    @ApiModelProperty(notes = "correo del cliente", name = "email",value = "jairyp90@gmail.com")
    private String email;

    @ApiModelProperty(notes = "dni del cliente", name = "dni",value = "12345678")
    private String dni;

    private String fecha_crea;

    @ApiModelProperty(notes = "fecha de nacimiento del cliente", name = "fecha_nacimiento",value = "1990-06-09")
    private String fecha_nacimiento;

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getFecha_crea() {
        return fecha_crea;
    }

    public void setFecha_crea(String fecha_crea) {
        this.fecha_crea = fecha_crea;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, true);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, true);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
