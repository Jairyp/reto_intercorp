package com.intercorp.retail.repository;

import com.intercorp.retail.core.domain.Indicators;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IndicatorRowMapper implements RowMapper<Indicators> {

    @Override
    public Indicators mapRow(ResultSet rs, int i) throws SQLException {
        Indicators indicators = new Indicators();

        indicators.setAnio(rs.getString("anio"));
        indicators.setMes(rs.getString("mes"));
        indicators.setCantidad(rs.getString("cantidad"));
        indicators.setNatalidad(rs.getString("natalidad"));
        return indicators;
    }

}
