package com.intercorp.retail.repository;

import com.intercorp.retail.core.domain.Customer;
import com.intercorp.retail.core.domain.Indicators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private static final Logger log = LoggerFactory.getLogger(CustomerRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final CustomerRowMapper customerRowMapper;
    private final IndicatorsRowMapper indicatorsRowMapper;

    public CustomerRepositoryImpl(JdbcTemplate jdbcTemplate, CustomerRowMapper customerRowMapper, IndicatorsRowMapper indicatorsRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.customerRowMapper = customerRowMapper;
        this.indicatorsRowMapper = indicatorsRowMapper;
    }

    @Override
    public List<Customer> getList() {
        List<Customer> list = jdbcTemplate.query("call GetAllCustomers",customerRowMapper);
        return list;
    }

    @Override
    public List<Customer> getByDniOrEmail(String dni,String email) {
        List<Customer> customers = jdbcTemplate.query("call GetAllCustomersByEmailOrDni(?,?)", new Object[] { dni,email }, customerRowMapper);
        return customers;
    }

    @Override
    public int save(Customer customer) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String now = sdf.format(new Date());

        return jdbcTemplate.update("call CustomersInsert(?,?,?,?,?,?);",new Object[] {
                customer.getNombre(),
                customer.getApellido(),
                customer.getEmail(),
                customer.getDni(),
                now,
                customer.getFecha_nacimiento()});
    }

    @Override
    public List<Indicators> getCountForYearAndMonth() {
        List<Indicators> indicators = jdbcTemplate.query("call GetCountCustomersForYearAndMonth()", indicatorsRowMapper);
        return indicators;
    }

    @Override
    public Indicators getMaxCustomerForYearOrMonth() {
        Indicators indicators = jdbcTemplate.queryForObject("call GetMaxCustomerForYearOrMonth()", indicatorsRowMapper);
        return indicators;
    }

    @Override
    public Indicators getMinCustomerForYearOrMonth() {
        Indicators indicators = jdbcTemplate.queryForObject("call GetMinCustomerForYearOrMonth()", indicatorsRowMapper);
        return indicators;
    }
}
