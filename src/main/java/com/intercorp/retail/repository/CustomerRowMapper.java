package com.intercorp.retail.repository;

import com.intercorp.retail.core.domain.Customer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

@Component
public class CustomerRowMapper implements RowMapper<Customer>{

    @Override
    public Customer mapRow(ResultSet rs, int i) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Customer customer = new Customer();
        customer.setId(rs.getInt("id"));
        customer.setNombre(rs.getString("nombre"));
        customer.setApellido(rs.getString("apellido"));
        customer.setEmail(rs.getString("email"));
        customer.setDni(rs.getString("dni"));
        customer.setFecha_crea(sdf.format(rs.getDate("fecha_crea")));
        customer.setFecha_nacimiento(sdf.format(rs.getDate("fecha_nacimiento")));
        return customer;
    }
}
