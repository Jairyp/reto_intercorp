package com.intercorp.retail.repository;

import com.intercorp.retail.core.domain.Customer;
import com.intercorp.retail.core.domain.Indicators;

import java.util.List;

public interface CustomerRepository {

    List<Customer> getByDniOrEmail(String dni,String email);
    List<Customer> getList();
    int save(Customer customer);
    List<Indicators> getCountForYearAndMonth();
    Indicators getMaxCustomerForYearOrMonth();
    Indicators getMinCustomerForYearOrMonth();


}
