package com.intercorp.retail.repository;

import com.intercorp.retail.core.domain.Indicators;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class IndicatorsRowMapper implements RowMapper<Indicators>{

    @Override
    public Indicators mapRow(ResultSet rs, int i) throws SQLException {
        Indicators indicators = new Indicators();

        indicators.setAnio(rs.getString("anio"));
        indicators.setMes(rs.getString("mes"));
        indicators.setCantidad(rs.getString("cantidad"));
        return indicators;
    }
}
